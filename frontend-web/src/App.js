import { Widget, Home, Dashboard, ErrorPage } from './pages'
import { NavBar } from 'components'
import { 
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'

const App = () => {
  return (
    <div style={{height: '100vh'}}> 
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<NavBar/>}>
            <Route index element={<Home/>}/>
            <Route path='/dashboard'>
              <Route index element={<Dashboard/>}/>
              <Route path=":user" element={<Dashboard/>}/>
            </Route>
          </Route>
          <Route path="/widget">
            <Route index element={<Widget/>}/>
            <Route path=":user" element={<Widget/>}/>
          </Route>
          <Route path="*" element={<ErrorPage/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
