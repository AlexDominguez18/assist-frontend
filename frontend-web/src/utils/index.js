export { default as getTextColor } from './getTextColor'
export { default as capitalize } from './capitalize'
export { default as getTime } from './getTime'