import React from 'react'
import logoLight from "assets/images/logo-light-icon.png"
import lightTextLogo from "assets/images/logo-light-text.png"
import userImage from 'assets/images/users/1.jpg'
import { Outlet } from 'react-router-dom'

const NavBar = () => {
  return (
    <>
      <header className="topbar">
          <nav className="navbar top-navbar navbar-expand-md navbar-light">
              {/* Nombre y logo */}
              <div className="navbar-header">
                  <a className="navbar-brand" href="index.html">
                    <b>
                      <img src={logoLight} alt="homepage" className="light-logo"/>
                    </b>
                    <span>
                      <img src={lightTextLogo} className="light-logo" alt="homepage" />
                    </span>
                  </a>
              </div>
              {/* Nombre y logo*/}

              {/* Cuerpo del header */}
              <div className="navbar-collapse">
                <ul className="navbar-nav my-lg-0">
                  {/* Notificaciones */}
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="mdi mdi-message"></i>
                      <div className="notify">
                        <span className="heartbit"></span>
                        <span className="point"></span>
                      </div>
                    </a>
                  </li>
                  {/* Notificaciones */}

                  {/* usuario */}
                  <li className="nav-item dropdown">
                      <a className="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src={userImage} alt="user" className="profile-pic" />
                      </a>
                  </li>
                  {/* usuario */}
                </ul>
              </div>
              {/* Cuerpo del header */}
          </nav>
      </header>
      <Outlet/>
    </>
  )
}

export default NavBar