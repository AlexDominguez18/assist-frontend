import { useState, useEffect, useContext, useRef } from 'react'
import Message from './Message'
import { UserContext } from '../context'
import { useSocket } from 'hooks'

const ChatArea = () => {
  const { user } = useContext(UserContext)
  const [msgs, setMsgs] = useState([])
  const { socket } = useSocket()
  const scrollRef = useRef(null)

  // Obtiene los mensajes del socket
  useEffect(() => {
    if (socket != null) {
      socket.on('connect', () => {
        socket.on('server-msg', args => {
          setMsgs(msgs => [...msgs, args])
        })
      })
    }
  }, [socket])

  useEffect(() => {
    scrollRef.current.scrollIntoView({behavior: 'smooth'})
  }, [msgs])

  return (
    <div className="chat-rbox h-100" style={{overflowY: 'auto'}}>
      <ul className="chat-list p-20">
        {
          msgs.map((e, i) => {
            return ( 
              <Message 
                key={e.id}
                reverse={e.user === user} 
                user={e.user}
                time={e.time}
                >
                {e.msg}
              </Message>
            )
          })
        }
        <li ref={scrollRef}/>
      </ul>
    </div>
  )
}

export default ChatArea