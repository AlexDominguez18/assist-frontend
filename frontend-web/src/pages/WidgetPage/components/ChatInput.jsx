import { useState, useContext, useRef } from 'react'
import { getTime } from 'utils'
import { nanoid } from 'nanoid'
import { UserContext } from '../context'
import { useSocket } from 'hooks'

const ChatInput = () => {
  const { user } = useContext(UserContext)
  const [message, setMessage] = useState("")
  const { socket } = useSocket()
  const focus = useRef(null)

  const handleSubmit = evt => {
    evt.preventDefault()
    if(message !== "") {
      // creamos el mensaje
      const newMsg = {
        id: nanoid(),
        user,
        time: getTime(),
        msg: message
      }
      // enviamos el mensaje al servidor
      socket.emit('client-msg', newMsg, "")
    }
    setMessage("")
    focus.current.focus()
  }

  return (
    <div className="card-body b-t">
        <form onSubmit={handleSubmit} className="d-flex w-100">
          <textarea
            ref={focus}
            autoFocus
            value={message}
            onChange={evt => setMessage(evt.target.value)} 
            placeholder="Escriba su mensaje aquí" 
            className="form-control b-0"/>
          <button type="submit" className="btn btn-info btn-circle btn-lg ml-3"
            style={{lineHeight: '100%'}}>
            <i className="fas fa-paper-plane"/>
          </button>
        </form>
    </div>
  )
}

export default ChatInput