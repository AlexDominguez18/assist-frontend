const ChatHeader = ({children}) => {
  return (
    <div className="chat-main-header">
        <div className="p-20 b-b">
            <h3 className="box-title">{children}</h3>
        </div>
    </div>
  )
}

export default ChatHeader