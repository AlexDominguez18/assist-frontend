import ChatArea from './ChatArea'
import ChatInput from './ChatInput'

const Chat = () => {
  return (
    <>
      <ChatArea/>
      <ChatInput/>
    </>
  )
}

export default Chat