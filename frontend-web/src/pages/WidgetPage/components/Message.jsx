import { UserImage } from 'components'

const Message = ({reverse, user, children, time}) => {
	const msgBg = reverse ? "bg-light-inverse" : "bg-light-info"

	return (
		<li className={reverse ? "reverse": ""}>
			<div>
				{ !reverse && <UserImage user={user}/> }
				<div className="chat-content">
					<h5>{user}</h5>
					<div className={`box m-t-20 ${msgBg}`} style={{maxWidth: "20ch"}}>
						{children}
					</div>
				</div>
				{ reverse && <UserImage user={user}/> }
			</div>
			<div className="chat-time">{time}</div>
		</li>
	)
}

export default Message