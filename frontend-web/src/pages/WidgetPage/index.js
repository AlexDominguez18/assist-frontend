import { ChatHeader, Chat } from './components'
import { UserContext } from './context'
import { useParams } from 'react-router-dom'

const Widget = () => {
  const user = useParams().user ?? 'Default User'

  return (
    <div className="d-flex h-100">
      <div className="card m-0 w-100">
        <ChatHeader>Sigi Chat</ChatHeader>
        <UserContext.Provider value={{user}}>
          <Chat/>
        </UserContext.Provider>
      </div>
    </div>
  )
}

export default Widget