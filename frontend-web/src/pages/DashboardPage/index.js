import Widget from '../WidgetPage'
import { Sidebar } from './components'
import styles from './styles.module.scss'

const Dashboard = () => {
  return (
    <div className="d-flex h-100">
      <Sidebar/>
      <div className={`container-fluid p-30 ${styles['chat-background']}`}>
        <Widget/>
      </div>  
    </div>
  )
}

export default Dashboard