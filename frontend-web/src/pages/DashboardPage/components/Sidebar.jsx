import UserCard from "./UserCard"
import SidebarOptions from "./SidebarOptions"
import styles from "../styles.module.scss"

const Sidebar = () => {
  return (
    <div className={styles.sidebar}>
      <UserCard/>
      <div className={styles.scroll}>
        <SidebarOptions/>
      </div>
    </div>
  )
}

export default Sidebar