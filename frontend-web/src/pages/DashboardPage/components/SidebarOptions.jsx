import NavDevider from './NavDevider'
import NavButton from './NavButton'
import NavTitle from "./NavTitle"

// estos botones serán las salas de chat
const options = [
  {
    icon: "mdi mdi-gauge",
    name: "CUCEI"
  },
  { 
    icon: "mdi mdi-gauge",
    name: "CUCEA"
  },
  { 
    icon: "mdi mdi-gauge",
    name: "CUAAD"
  }
]

const SidebarOptions = () => {
  return (
    <nav className="sidebar-nav p-15 h-100">
        <ul id="sidebarnav">
          <NavTitle>SERVIDORES</NavTitle>
          { 
            options.map(e => {
                return (
                <NavButton key={e.name}>
                  <i className={e.icon}></i>
                  <span>{e.name}</span>
                </NavButton>
              )
            })
          }
          <NavDevider/>
        </ul>
    </nav>
  )
}

export default SidebarOptions