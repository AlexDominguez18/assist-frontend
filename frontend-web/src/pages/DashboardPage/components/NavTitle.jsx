const NavTitle = ({ children }) => {
  return <li className="nav-small-cap">{ children }</li>
}

export default NavTitle