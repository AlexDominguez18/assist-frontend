import { UserImage } from 'components'
import { useParams } from 'react-router-dom'
import styles from '../styles.module.scss'

const UserCard = () => {
  const user = useParams().user ?? 'Default User'
  return (
    <div className={`user-profile ${styles.userBg}`}>
        <div className="profile-img">
          <UserImage user={user} styling={{width: '50px', lineHeight: '50px'}}/>
        </div>
        <button className={styles["dropdown-toggle"]}>
          { user }
        </button>
    </div>
  )
}

export default UserCard