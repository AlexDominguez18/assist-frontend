export { default as Widget } from './WidgetPage'
export { default as Home } from './HomePage'
export { default as ErrorPage } from './ErrorPage'
export { default as Dashboard } from './DashboardPage'