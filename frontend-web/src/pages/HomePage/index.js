import { useState } from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  const [user, setUser] = useState("")
  return (
    <div style={{textAlign: 'center'}}>
      <h1>Home</h1>
      <input 
        type="text" 
        placeholder="usuario"
        value={user}
        onChange={evt => setUser(evt.target.value)}
      />
      <br/>
      <Link to={`/dashboard/${user}`}>
        dashboard
      </Link>
      <br/>
      <Link to={`/widget/${user}`}>
        Widget Chat
      </Link>
    </div>
  )
}

export default Home