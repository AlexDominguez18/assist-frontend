(() => {
  const script = document.currentScript;

  const loadWidget = () => {
      // creación del botón que desplegará el widget
      const widgetButton = document.createElement('button');
      const icon = document.createElement('i');

      // estilos para el botón
      widgetButton.classList.add("btn", "btn-primary", "widget");
      icon.classList.add("fa", "fa-paper-plane", "fa-lg");

      widgetButton.appendChild(icon);
      
      // contenedor del iframe y iframe
      const widget = document.createElement("div");
      const iframe = document.createElement("iframe");

      // estilos del widget
      widget.className = "widget-container";
      iframe.className = "widget-content";
      
      widget.appendChild(iframe);
      
      // añadimos un evento al boton para desplegar el iframe
      widgetButton.addEventListener("click", () => {
          const display = widget.style.display;
          widget.style.display = display === "none" ? "block": "none";
      })

      // mostramos el boton hasta que el iframe haya cargado
      iframe.addEventListener("load", () => {
        widgetButton.style.display = "block";
        widget.style.display = "none";
      });

      const user = script.getAttribute("user");
      const widgetUrl = `http://localhost:3000/widget/${user}`;
      
      iframe.src = widgetUrl;

      // añadimos el widget y el botón al body
      document.body.appendChild(widget);
      document.body.appendChild(widgetButton);      
  }
  
  if ( document.readyState === "complete" ) {
    loadWidget();
  } else {
    document.addEventListener("readystatechange", () => {
      if ( document.readyState === "complete" )
        loadWidget();
    });
  }
})();